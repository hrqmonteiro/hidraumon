export default function Card1 (props) {
  return (
    <div>
      <h3 className="text-uppercase">Linha</h3>
      <h3 className="text-uppercase"><strong>{props.title}</strong></h3>
      <p>{props.description}</p>
    </div>
  )
}
