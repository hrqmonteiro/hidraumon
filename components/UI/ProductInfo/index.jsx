import { Col, Row } from "react-bootstrap";
import Lightbox from "../Lightbox";

export default function ProductInfo (props) {
  return (
    <>
      <span id="product-description">Descritivo técnico:</span>
      <br />
      <br />
      <ul>
        <li>
          {props.lineone}
        </li>
        <li>
          {props.linetwo}
        </li>
        <li>
          {props.linethree}
        </li>
        <li>
          {props.linefour}
        </li>
        <li>
          {props.linefive}
        </li>
        <li>
          {props.linesix}
        </li>
        <li>
          {props.lineseven}
        </li>
        <li>
          {props.lineeight}
        </li>
        <li>
          {props.linenine}
        </li>
        <li>
          {props.lineten}
        </li>
        <li>
          {props.lineeleven}
        </li>
        <li>
          {props.linetwelve}
        </li>
        <li>
          {props.linethirteen}
        </li>
        <li>
          {props.linefourteen}
        </li>
        <li>
          {props.linefifteen}
        </li>
        <li>
          {props.linesixteen}
        </li>

      </ul>
      <br />
      <br />
      <span className="text-uppercase">
        <strong>Ref: </strong>
        <span className="reference"><em>{props.reference}</em></span>
      </span>
      <br />
      <span className="text-uppercase">
        <strong>Marca: </strong><span className="underline--magical"><strong><em> Hidraumon®</em></strong></span>
      </span>
      </>
  )
}
