import lgZoom from "lightgallery/plugins/zoom";
import dynamic from "next/dynamic";
import { Col, Row } from "react-bootstrap";

const LightGallery = dynamic(() => import
('lightgallery/react'), {
    ssr: false
  });

export default function Lightbox(props) {
  return (
    <LightGallery mode="lg-fade" plugins={[lgZoom]}>
      <a
        className="gallery-item"
        data-src={props.imgone}
        data-sub-html={props.descone}
      >
        <img
          className="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-1 img-fluid"
          src={props.imgone}
          />
      </a>
      <a
        className="gallery-item"
        data-src={props.imgtwo}
        data-sub-html={props.descone}
      >
        <img
          className="col-lg-6 col-md-6 col-sm-6 col-xs-6 p-1 img-fluid"
          src={props.imgtwo}
          />
      </a>
      <a
        className="gallery-item"
        data-src={props.imgthree}
        data-sub-html={props.descone}
      >
        <img
          className="col-lg-6 col-md-6 col-sm-4 col-xs-6 p-1 img-fluid"
          src={props.imgthree}
          />
      </a>
    </LightGallery>
  )
}
