import { Col, Container, Row } from "react-bootstrap";
import EmblaCarousel from "../../UI/EmblaCarousel";
import MobileCarousel from "../../UI/MobileCarousel";
import Card1 from "../../UI/Card1";

export default function Slider() {
  return (
    <>
      <section id="home">
        <Container>
          <Row>
            <Col lg={6} md={12} sm={12} xs={12} id="header-top">
              <h1 className="mb-5" data-aos="fade-up" data-aos-delay="100">
                <strong>Excelência</strong> na <strong>produção</strong> de
                equipamentos <strong>hidráulicos.</strong>
              </h1>
              <p data-aos="fade-up" data-aos-delay="200">
                <span id="copy-1">
                  Estamos aqui para <strong>oferecer o melhor! </strong>
                  Nossos clientes não pedem por menos.
                </span>
              </p>
              <div id="copy-2">
                <Row data-aos="zoom-in" data-aos-delay="400" className="py-4">
                  <Col lg={4} md={4} sm={12} xs={12} className="mb-2">
                    <p className="numm-1">Mais de</p>
                    <p id="number">10.000</p>
                    <p>Clientes em todo o território nacional!</p>
                  </Col>
                  <Col lg={4} md={4} sm={12} xs={12} className="mb-2">
                    <p className="numm-1">Mais de</p>
                    <p id="number">25</p>
                    <p>Anos de experiência no segmento hidráulico!</p>
                  </Col>
                  <Col lg={4} md={4} sm={12} xs={12} className="mb-2">
                    <p className="numm-1">Atuando em</p>
                    <p id="number">20</p>
                    <p>Estados do Brasil!</p>
                  </Col>
                </Row>
              </div>
            </Col>
          </Row>
          <Row>
            <EmblaCarousel /> 
            <MobileCarousel />
          </Row>
        </Container>
      </section>
    </>
  );
}
