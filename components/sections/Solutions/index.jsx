import { Card, Col, Container, Row } from "react-bootstrap";
import Card1 from "../../UI/Card1";
import EmblaCarousel from "../../UI/EmblaCarousel";
import { MdKeyboardArrowRight } from "react-icons/md";
import Link from "next/link";

export default function Solutions() {
  return (
    <section id="solutions">
      <Container>
        
        <Row className="py-6">
          <Col lg={3} md={3} sm={12} xs={12}>
            <h2 className="mb-5" data-aos="fade-up">
              Nossas soluções
              <br />
              <strong>são definitivas!</strong>
            </h2>
            <div data-aos="fade-up" data-aos-delay="200">
              <p>
                Atuamos há mais de 25 anos no mercado e somos líderes no ramo de
                equipamentos hidráulicos.
              </p>
              <p>
                Estamos qualificados para te fornecer equipamentos hidráulicos
                em alta qualidade, desde o material utilizado até o uso final.
              </p>
            </div>
            <div className="my-5" data-aos="fade-up" data-aos-delay="200">
              <a href="/catalogo">
                <button className="button-secondary">
                  Confira nosso catálogo! <MdKeyboardArrowRight />
                </button>
              </a>
            </div>
          </Col>
          <Col lg={3} md={3} sm={12} xs={12} className="my-3" data-aos="fade-up" data-aos-delay="500">
            <Link href="/catalogo">
              <a>
                <Card>
                  <Card.Img variant="top" src="/images/macaco.png" />
                  <Card.Body>
                    <Card.Title>Macacos Hidráulicos</Card.Title>
                    <Card.Text>
                      Temos uma linha complete: macacos hidráulicos, garrafa, moleiro, carroceria, contêiner e macacos longa distância.
                    </Card.Text>
                  </Card.Body>
                </Card>
              </a>
            </Link>
          </Col>
          <Col lg={3} md={3} sm={12} xs={12} className="my-3" data-aos="fade-up" data-aos-delay="600">
            <Link href="/catalogo">
              <a>
                <Card>
                  <Card.Img variant="top" src="/images/prensa.png" />
                  <Card.Body>
                    <Card.Title>Prensas</Card.Title>
                    <Card.Text>
                    Temos uma linha completa de prensas para diversas aplicações de prensagem, modelagem ou corte.
                    </Card.Text>
                  </Card.Body>
                </Card>
              </a>
            </Link>
          </Col>
          <Col lg={3} md={3} sm={12} xs={12} className="my-3" data-aos="fade-up" data-aos-delay="700">
            <Link href="/catalogo">
              <a>
                <Card>
                  <Card.Img variant="top" src="/images/cilindro.png" />
                  <Card.Body>
                    <Card.Title>Cilindros e Bombas</Card.Title>
                    <Card.Text>
                    Cilindros, bombas motorizadas, bombas eletro-hidráulicas e outros, para as mais diversas aplicações!
                    </Card.Text>
                  </Card.Body>
                </Card>
              </a>
            </Link>
          </Col>
        </Row>
      </Container>
    </section>
  );
}
