import { Col, Row } from "react-bootstrap";
import CatalogCard from "../UI/CatalogCard";
import Link from "next/link";

const prensas = [
  {
    name: "Prensa Hidropneumática 30 toneladas",
    link: "prensa-hidropneumatica-30-toneladas",
    description:
      "As informações técnicas variam de acordo com a tonelagem e acionamento do produto.",
    image: "prensa-hidropneumatica-30-toneladas/1.jpg",
  },
  {
    name: "Prensa Manual 15 Toneladas",
    link: "prensa-manual-15-toneladas",
    description:
      "As informações técnicas variam de acordo com a tonelagem e acionamento do produto.",
    image: "prensa-manual-15-toneladas/1.jpg",
  },
  {
    name: "Prensa Manual 30 Toneladas",
    link: "prensa-manual-30-toneladas",
    description:
      "As informações técnicas variam de acordo com a tonelagem e acionamento do produto.",
    image: "prensa-manual-30-toneladas/1.jpg",
  },
  {
    name: "Prensa Manual NR-12 200 Toneladas",
    link: "prensa-manual-nr12-200-toneladas",
    description:
      "As informações técnicas variam de acordo com a tonelagem e acionamento do produto.",
    image: "prensa-manual-nr12-200-toneladas/1.jpg",
  },
  {
    name: "Prensa Manual 60 Toneladas",
    link: "prensa-manual-60-toneladas",
    description:
      "As informações técnicas variam de acordo com a tonelagem e acionamento do produto.",
    image: "prensa-manual-60-toneladas/1.jpg",
  },
  {
    name: "Prensa Manual 100 toneladas",
    link: "prensa-manual-100-toneladas",
    description:
      "As informações técnicas variam de acordo com a tonelagem e acionamento do produto.",
    image: "prensa-manual-100-toneladas/1.jpg",
  },
  {
    name: "Prensa Manual 200 toneladas",
    link: "prensa-manual-200-toneladas",
    description:
      "As informações técnicas variam de acordo com a tonelagem e acionamento do produto.",
    image: "prensa-manual-200-toneladas/1.jpg",
  },
  {
    name: "Prensa Motorizada 30 Toneladas",
    link: "prensa-motorizada-30-toneladas",
    description:
      "As informações técnicas variam de acordo com a tonelagem e acionamento do produto.",
    image: "prensa-motorizada-30-toneladas/1.jpg",
  },
  {
    name: "Prensa Motorizada 45 Toneladas",
    link: "prensa-motorizada-45-toneladas",
    description:
      "As informações técnicas variam de acordo com a tonelagem e acionamento do produto.",
    image: "prensa-motorizada-45-toneladas/1.jpg",
  },
  {
    name: "Prensa Motorizada 100 Toneladas",
    link: "prensa-motorizada-100-toneladas",
    description:
      "As informações técnicas variam de acordo com a tonelagem e acionamento do produto.",
    image: "prensa-motorizada-100-toneladas/1.jpg",
  },
  {
    name: "Prensa Motorizada NR-12 100 Toneladas",
    link: "prensa-motorizada-nr-12-100-toneladas",
    description:
      "As informações técnicas variam de acordo com a tonelagem e acionamento do produto.",
    image: "prensa-motorizada-nr-12-100-toneladas/1.jpg",
  },
];

const catalogoPrensas = prensas.map((prensa) => (
  <Col lg={4} md={4} sm={12} xs={12} className="mb-3">
    <Link to href={"/catalogo/prensas/" + prensa.link}>
      <a>
        <CatalogCard
          title={prensa.name}
          description={prensa.description}
          image={"/images/produtos/prensas/" + prensa.image}
        />
      </a>
    </Link>
  </Col>
));

export default function Prensas() {
  return (
    <>
      <Row>{catalogoPrensas}</Row>
    </>
  );
}
