import { Col, Container, Row, } from "react-bootstrap";
import Head from "next/head";
import ProductInfo from "../../../components/UI/ProductInfo";
import { BiRadioCircle } from "react-icons/bi";
import lgZoom from "lightgallery/plugins/zoom";
import dynamic from "next/dynamic";

const LightGallery = dynamic(() => import
('lightgallery/react'), {
    ssr: false
  });

export default function Desparafusadeira () {
  return (
    <>
      <Head>
        <title>Desparafusadeira | Hidraumon Máquinas</title>
      </Head>

      <section className="product-info">
        <Container>
          <Row className="py-5">
            <Col lg={12} md={12} sm={12} xs={12}>
              <h2 data-aos="fade-up">
              Desparafusadeira
              </h2>
              <br />
              <p data-aos="fade-up" data-aos-delay="300">
              </p>
            </Col>
          </Row>
          <Row>
            <Col lg={6} md={6} sm={12} xs={12} className="mb-5" data-aos="fade-up" data-aos-delay="500">
              <LightGallery mode="lg-fade" plugins={[lgZoom]}>
                <a
                  className="gallery-item"
                  data-src="/images/produtos/desparafusadeiras/desparafusadeira/1.jpg"
                  data-sub-html="<h4>Desparafusadeira</h4><p>Hidraumon Máquinas</p>"
                >
                  <img
                    className="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-1 img-fluid"
                    src="/images/produtos/desparafusadeiras/desparafusadeira/1.jpg"
                    />
                </a>
                <a
                  className="gallery-item"
                  data-src="/images/produtos/desparafusadeiras/desparafusadeira/2.jpg"
                  data-sub-html="<h4>Desparafusadeira</h4><p>Hidraumon Máquinas</p>"
                >
                  <img
                    className="col-lg-4 col-md-4 col-sm-4 col-xs-4 p-1 img-fluid"
                    src="/images/produtos/desparafusadeiras/desparafusadeira/2.jpg"
                    />
                </a>
                <a
                  className="gallery-item"
                  data-src="/images/produtos/desparafusadeiras/desparafusadeira/3.jpg"
                  data-sub-html="<h4>Desparafusadeira</h4><p>Hidraumon Máquinas</p>"
                >
                  <img
                    className="col-lg-4 col-md-4 col-sm-4 col-xs-4 p-1 img-fluid"
                    src="/images/produtos/desparafusadeiras/desparafusadeira/3.jpg"
                    />
                </a>
                <a
                  className="gallery-item"
                  data-src="/images/produtos/desparafusadeiras/desparafusadeira/4.jpg"
                  data-sub-html="<h4>Desparafusadeira</h4><p>Hidraumon Máquinas</p>"
                >
                  <img
                    className="col-lg-4 col-md-4 col-sm-4 col-xs-4 p-1 img-fluid"
                    src="/images/produtos/desparafusadeiras/desparafusadeira/4.jpg"
                    />
                </a>
              </LightGallery>
            </Col>
            <Col lg={6} md={6} sm={12} xs={12} data-aos="fade-up" data-aos-delay="800">
              <ProductInfo
                lineone={[
                  <BiRadioCircle />,
                  " Controle de aperto realizado através do impulso dado pela chave elétrica"
                ]}
                linetwo={[
                  <BiRadioCircle />,
                  " Cabeça giratória 360°"
                ]}
                linethree={[
                  <BiRadioCircle />,
                  " Tensão: trifásico"
                ]}
                linefour={[
                  <BiRadioCircle />,
                  " Velocidade de rotação: 16RPM"
                ]}
                linefive={[
                  <BiRadioCircle />,
                  " Motor: 3CV"
                ]}
                linesix={[
                  <BiRadioCircle />,
                  " Torque nominal 400 kg/m"
                ]}
                lineseven={[
                  <BiRadioCircle />,
                  " Torque máximo 420 kg/m"
                ]}
                lineeight={[
                  <BiRadioCircle />,
                  " Voltagem: 220V/380V"
                ]}
                linenine={[
                  <BiRadioCircle />,
                  " Redutor com coroa e sem fim banhado a óleo"
                ]}
                lineten={[
                  <BiRadioCircle />,
                  " Junta universal para compensação de desalinhamento de cachimbo (soquete)"
                ]}
                lineeleven={[
                  <BiRadioCircle />,
                  " Chave elétrica reversora de impulso"
                ]}
                linetwelve={[
                  <BiRadioCircle />,
                  " Base em ferro fundido"
                ]}
                linethirteen={[
                  <BiRadioCircle />,
                  " Rotação de saída no soquete: 38RPM"
                ]}
                linefourteen={[
                  <BiRadioCircle />,
                  " Possui duas rodas de 175mm de diâmetro"
                ]}
                linefifteen={[
                  <BiRadioCircle />,
                  " Dimensões da máquina: C-1900mm x L-360mm x A-290mm"
                ]}
                linesixteen={[
                  <BiRadioCircle />,
                  " Peso: 75kg"
                ]}
                />
            </Col>
          </Row>
        </Container>
      </section>
      </>
  );
}
