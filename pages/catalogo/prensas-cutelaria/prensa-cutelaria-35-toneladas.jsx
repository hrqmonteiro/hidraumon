import { Col, Container, Row } from "react-bootstrap";
import Head from "next/head";
import ProductInfo from "../../../components/UI/ProductInfo";
import { BiRadioCircle } from "react-icons/bi";
import lgZoom from "lightgallery/plugins/zoom";
import dynamic from "next/dynamic";

const LightGallery = dynamic(() => import("lightgallery/react"), {
  ssr: false,
});

export default function PrensaOne() {
  return (
    <>
      <Head>
        <title>Prensa Cutelaria 35 Toneladas | Hidraumon Máquinas</title>
      </Head>

      <section className="product-info">
        <Container>
          <Row className="py-5">
            <Col lg={12} md={12} sm={12} xs={12}>
              <h2 data-aos="fade-up">
                Prensa Cutelaria
                <br />
                <strong>35 Toneladas</strong>
              </h2>
              <br />
              <p data-aos="fade-up" data-aos-delay="300"></p>
            </Col>
          </Row>
          <Row>
            <Col
              lg={6}
              md={6}
              sm={12}
              xs={12}
              className="mb-5"
              data-aos="fade-up"
              data-aos-delay="500"
            >
              <LightGallery mode="lg-fade" plugins={[lgZoom]}>
                <a
                  className="gallery-item"
                  data-src="/images/produtos/prensas-cutelaria/prensa-cutelaria-35-toneladas/1.png"
                  data-sub-html="<h4>Prensa Cutelaria 35 Toneladas</h4><p>Hidraumon Máquinas</p>"
                >
                  <img
                    className="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-1 img-fluid"
                    src="/images/produtos/prensas-cutelaria/prensa-cutelaria-35-toneladas/1.png"
                  />
                </a>
                <a
                  className="gallery-item"
                  data-src="/images/produtos/prensas-cutelaria/prensa-cutelaria-35-toneladas/2.png"
                  data-sub-html="<h4>Prensa Cutelaria 35 Toneladas</h4><p>Hidraumon Máquinas</p>"
                >
                  <img
                    className="col-lg-4 col-md-4 col-sm-4 col-xs-4 p-1 img-fluid"
                    src="/images/produtos/prensas-cutelaria/prensa-cutelaria-35-toneladas/2.png"
                  />
                </a>
                <a
                  className="gallery-item"
                  data-src="/images/produtos/prensas-cutelaria/prensa-cutelaria-35-toneladas/3.png"
                  data-sub-html="<h4>Prensa Cutelaria 35 Toneladas</h4><p>Hidraumon Máquinas</p>"
                >
                  <img
                    className="col-lg-4 col-md-4 col-sm-4 col-xs-4 p-1 img-fluid"
                    src="/images/produtos/prensas-cutelaria/prensa-cutelaria-35-toneladas/3.png"
                  />
                </a>
                <a
                  className="gallery-item"
                  data-src="/images/produtos/prensas-cutelaria/prensa-cutelaria-35-toneladas/4.png"
                  data-sub-html="<h4>Prensa Cutelaria 35 Toneladas</h4><p>Hidraumon Máquinas</p>"
                >
                  <img
                    className="col-lg-4 col-md-4 col-sm-4 col-xs-4 p-1 img-fluid"
                    src="/images/produtos/prensas-cutelaria/prensa-cutelaria-35-toneladas/4.jpg"
                  />
                </a>
              </LightGallery>
            </Col>
            <Col
              lg={6}
              md={6}
              sm={12}
              xs={12}
              data-aos="fade-up"
              data-aos-delay="800"
            >
              <ProductInfo
                lineone={[
                  <BiRadioCircle />,
                  " As informações técnicas variam de acordo com a tonelagem do produto.",
                ]}
              />
            </Col>
          </Row>
        </Container>
      </section>
    </>
  );
}
