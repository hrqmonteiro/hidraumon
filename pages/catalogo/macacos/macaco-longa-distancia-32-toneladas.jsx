import { Col, Container, Row, } from "react-bootstrap";
import Head from "next/head";
import ProductInfo from "../../../components/UI/ProductInfo";
import { BiRadioCircle } from "react-icons/bi";
import lgZoom from "lightgallery/plugins/zoom";
import dynamic from "next/dynamic";

const LightGallery = dynamic(() => import
('lightgallery/react'), {
    ssr: false
  });

export default function MacacoHLonga () {
  return (
    <>
      <Head>
        <title>Macaco Longa Distância 32 Toneladas | Hidraumon Máquinas</title>
      </Head>

      <section className="product-info">
        <Container>
          <Row className="py-5">
            <Col lg={12} md={12} sm={12} xs={12}>
              <h2 data-aos="fade-up">
              Macaco Longa Distância
                <br />
                <strong>32 Toneladas</strong>
              </h2>
              <br />
              <p data-aos="fade-up" data-aos-delay="300">
              Desenvolvido para utilização em oficinas, postos de molas e similares em geral.
              <br />
              Seu grande diferencial é a segurança, por ser um equipamento que possibilita ser usado à distância de 2 metros ele é mais seguro que os macacos convencionais.
              </p>
            </Col>
          </Row>
          <Row>
            <Col lg={6} md={6} sm={12} xs={12} className="mb-5" data-aos="fade-up" data-aos-delay="500">
              <LightGallery mode="lg-fade" plugins={[lgZoom]}>
                <a
                  className="gallery-item"
                  data-src="/images/produtos/macacos/macaco-longa-distancia-32-toneladas/1.jpg"
                  data-sub-html="<h4>Macaco Longa Distância 32 Toneladas</h4><p>Hidraumon Máquinas</p>"
                >
                  <img
                    className="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-1 img-fluid"
                    src="/images/produtos/macacos/macaco-longa-distancia-32-toneladas/1.jpg"
                    />
                </a>
              </LightGallery>
            </Col>
            <Col lg={6} md={6} sm={12} xs={12} data-aos="fade-up" data-aos-delay="800">

              <ProductInfo
                lineone={[
                  <BiRadioCircle />,
                  " As informações técnicas variam de acordo com a tonelagem do produto."
                ]}
                />
            </Col>
          </Row>
        </Container>
      </section>
      </>
  );
}
