import { Col, Container, Row, } from "react-bootstrap";
import Head from "next/head";
import ProductInfo from "../../../components/UI/ProductInfo";
import { BiRadioCircle } from "react-icons/bi";
import lgZoom from "lightgallery/plugins/zoom";
import dynamic from "next/dynamic";

const LightGallery = dynamic(() => import
('lightgallery/react'), {
    ssr: false
  });

export default function MacacoTrintaedoisseicentos () {
  return (
    <>
      <Head>
        <title>Macaco Moleiro Manual 32 Toneladas - 32600 | Hidraumon Máquinas</title>
      </Head>

      <section className="product-info">
        <Container>
          <Row className="py-5">
            <Col lg={12} md={12} sm={12} xs={12}>
              <h2 data-aos="fade-up">
              Macaco Moleiro Manual
                <br />
                <strong>32 Toneladas - 32600</strong>
              </h2>
              <br />
              <p data-aos="fade-up" data-aos-delay="300">
              Desenvolvido para utilização em oficinas, postos de molas e similares em geral.
              <br />
              Disponível em diversas alturas para atender as variações de chassis e carrocerias.
              <br />
              Equipamentos com acionamento manual.
              </p>
            </Col>
          </Row>
          <Row>
            <Col lg={6} md={6} sm={12} xs={12} className="mb-5" data-aos="fade-up" data-aos-delay="500">
              <LightGallery mode="lg-fade" plugins={[lgZoom]}>
                <a
                  className="gallery-item"
                  data-src="/images/produtos/macacos/macaco-moleiro-manual-32-toneladas-32600/1.jpg"
                  data-sub-html="<h4>Macaco Moleiro Manual 32 Toneladas</h4><p>Hidraumon Máquinas</p>"
                >
                  <img
                    className="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-1 img-fluid"
                    src="/images/produtos/macacos/macaco-moleiro-manual-32-toneladas-32600/1.jpg"
                    />
                </a>
                <a
                  className="gallery-item"
                  data-src="/images/produtos/macacos/macaco-moleiro-manual-32-toneladas-32600/2.jpg"
                  data-sub-html="<h4>Macaco Moleiro Manual 32 Toneladas</h4><p>Hidraumon Máquinas</p>"
                >
                  <img
                    className="col-lg-6 col-md-6 col-sm-6 col-xs-6 p-1 img-fluid"
                    src="/images/produtos/macacos/macaco-moleiro-manual-32-toneladas-32600/2.jpg"
                    />
                </a>
                <a
                  className="gallery-item"
                  data-src="/images/produtos/macacos/macaco-moleiro-manual-32-toneladas-32600/3.jpg"
                  data-sub-html="<h4>Macaco Moleiro Manual 32 Toneladas</h4><p>Hidraumon Máquinas</p>"
                >
                  <img
                    className="col-lg-6 col-md-6 col-sm-6 col-xs-6 p-1 img-fluid"
                    src="/images/produtos/macacos/macaco-moleiro-manual-32-toneladas-32600/3.jpg"
                    />
                </a>
              </LightGallery>
            </Col>
            <Col lg={6} md={6} sm={12} xs={12} data-aos="fade-up" data-aos-delay="800">

              <ProductInfo
                lineone={[
                  <BiRadioCircle />,
                  " As informações técnicas variam de acordo com a tonelagem do produto."
                ]}
                />
            </Col>
          </Row>
        </Container>
      </section>
      </>
  );
}
