import { Col, Container, Row, } from "react-bootstrap";
import Head from "next/head";
import ProductInfo from "../../../components/UI/ProductInfo";
import { BiRadioCircle } from "react-icons/bi";
import lgZoom from "lightgallery/plugins/zoom";
import dynamic from "next/dynamic";

const LightGallery = dynamic(() => import
('lightgallery/react'), {
    ssr: false
  });

export default function MacacoTrintaedois () {
  return (
    <>
      <Head>
        <title>Macaco Garrafa Manual 32 Toneladas | Hidraumon Máquinas</title>
      </Head>

      <section className="product-info">
        <Container>
          <Row className="py-5">
            <Col lg={12} md={12} sm={12} xs={12}>
              <h2 data-aos="fade-up">
              Macaco Garrafa Manual
                <br />
                <strong>32 Toneladas</strong>
              </h2>
              <br />
              <p data-aos="fade-up" data-aos-delay="300">
              Macaco hidráulico com acionamento manual desenvolvido para utilização em oficinas, borracharias e serviços em geral.
              <br />
              Desenvolvido para elevação de veículos e cargas, possui injetor com acionamento manual.
              </p>
            </Col>
          </Row>
          <Row>
            <Col lg={6} md={6} sm={12} xs={12} className="mb-5" data-aos="fade-up" data-aos-delay="500">
              <LightGallery mode="lg-fade" plugins={[lgZoom]}>
                <a
                  className="gallery-item"
                  data-src="/images/produtos/macacos/macaco-garrafa-manual-32-toneladas/1.jpg"
                  data-sub-html="<h4>Macaco Garrafa Manual 32 Toneladas</h4><p>Hidraumon Máquinas</p>"
                >
                  <img
                    className="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-1 img-fluid"
                    src="/images/produtos/macacos/macaco-garrafa-manual-32-toneladas/1.jpg"
                    />
                </a>
                <a
                  className="gallery-item"
                  data-src="/images/produtos/macacos/macaco-garrafa-manual-32-toneladas/2.jpg"
                  data-sub-html="<h4>Macaco Garrafa Manual Toneladas</h4><p>Hidraumon Máquinas</p>"
                >
                  <img
                    className="col-lg-6 col-md-6 col-sm-6 col-xs-6 p-1 img-fluid"
                    src="/images/produtos/macacos/macaco-garrafa-manual-32-toneladas/2.jpg"
                    />
                </a>
              </LightGallery>
            </Col>
            <Col lg={6} md={6} sm={12} xs={12} data-aos="fade-up" data-aos-delay="800">

              <ProductInfo
                lineone={[
                  <BiRadioCircle />,
                  " Capacidade: 32 toneladas"
                ]}
                linetwo={[
                  <BiRadioCircle />,
                  " Altura fechado: 250mm"
                ]}
                linethree={[
                  <BiRadioCircle />,
                  " Curso: 140mm"
                ]}
                linefour={[
                  <BiRadioCircle />,
                  " Fuso: 80mm"
                ]}
                linefive={[
                  <BiRadioCircle />,
                  " Altura total: 470mm"
                ]}
                linesix={[
                  <BiRadioCircle />,
                  " Peso: 18kg"
                ]}
                lineseven={[
                  <BiRadioCircle />,
                  " Garantia: 1 ano"
                ]}
                />
            </Col>
          </Row>
        </Container>
      </section>
      </>
  );
}
