import { Col, Container, Row, } from "react-bootstrap";
import Head from "next/head";
import ProductInfo from "../../../components/UI/ProductInfo";
import { BiRadioCircle } from "react-icons/bi";
import lgZoom from "lightgallery/plugins/zoom";
import dynamic from "next/dynamic";

const LightGallery = dynamic(() => import
('lightgallery/react'), {
    ssr: false
  });

export default function Destalonador () {
  return (
    <>
      <Head>
        <title>Destalonador de Pneus | Hidraumon Máquinas</title>
      </Head>

      <section className="product-info">
        <Container>
          <Row className="py-5">
            <Col lg={12} md={12} sm={12} xs={12}>
              <h2 data-aos="fade-up">
              Destalonador
                <br />
                <strong>De pneus</strong>
              </h2>
              <br />
              <p data-aos="fade-up" data-aos-delay="300">
              Desenvolvido para descolagem de talão em pneus agrícolas, caminhões e similares.
                <br />
                Possui conjunto hidráulico com cilindro dupla ação e bomba hidropneumática.
              </p>
            </Col>
          </Row>
          <Row>
            <Col lg={6} md={6} sm={12} xs={12} className="mb-5" data-aos="fade-up" data-aos-delay="500">
              <LightGallery mode="lg-fade" plugins={[lgZoom]}>
                <a
                  className="gallery-item"
                  data-src="/images/produtos/destalonadores/destalonador-de-pneus/1.jpg"
                  data-sub-html="<h4>Destalonador de Pneus</h4><p>Hidraumon Máquinas</p>"
                >
                  <img
                    className="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-1 img-fluid"
                    src="/images/produtos/destalonadores/destalonador-de-pneus/1.jpg"
                    />
                </a>
                <a
                  className="gallery-item"
                  data-src="/images/produtos/destalonadores/destalonador-de-pneus/2.jpg"
                  data-sub-html="<h4>Destalonador de Pneus</h4><p>Hidraumon Máquinas</p>"
                >
                  <img
                    className="col-lg-6 col-md-6 col-sm-6 col-xs-6 p-1 img-fluid"
                    src="/images/produtos/destalonadores/destalonador-de-pneus/2.jpg"
                    />
                </a>
                <a
                  className="gallery-item"
                  data-src="/images/produtos/destalonadores/destalonador-de-pneus/3.jpg"
                  data-sub-html="<h4>Destalonador de Pneus</h4><p>Hidraumon Máquinas</p>"
                >
                  <img
                    className="col-lg-6 col-md-6 col-sm-6 col-xs-6 p-1 img-fluid"
                    src="/images/produtos/destalonadores/destalonador-de-pneus/3.jpg"
                    />
                </a>

              </LightGallery>
            </Col>
            <Col lg={6} md={6} sm={12} xs={12} data-aos="fade-up" data-aos-delay="800">

              <ProductInfo
                lineone={[
                  <BiRadioCircle />,
                  " Capacidade: 15 toneladas"
                ]}
                linetwo={[
                  <BiRadioCircle />,
                  " Largura máxima aberto: 800mm"
                ]}
                linethree={[
                  <BiRadioCircle />,
                  " Largura mínima fechado: 210mm"
                ]}
                linefour={[
                  <BiRadioCircle />,
                  " Consumo de ar: 10PCM"
                ]}
                linefive={[
                  <BiRadioCircle />,
                  " Compressor recomendado: 10 pés/175 libras"
                ]}
                linesix={[
                  <BiRadioCircle />,
                  " Reservatório de óleo: 2 litros/Óleo 32W"
                ]}
                lineseven={[
                  <BiRadioCircle />,
                  " Peso: 78kg"
                ]}
                lineeight={[
                  <BiRadioCircle />,
                  " Garantia: 1 ano"
                ]}
                reference="PRO C" 
                />
            </Col>
          </Row>
        </Container>
      </section>
      </>
  );
}
