import { Col, Container, Row, } from "react-bootstrap";
import Head from "next/head";
import ProductInfo from "../../../components/UI/ProductInfo";
import { BiRadioCircle } from "react-icons/bi";
import lgZoom from "lightgallery/plugins/zoom";
import dynamic from "next/dynamic";

const LightGallery = dynamic(() => import
('lightgallery/react'), {
    ssr: false
  });

export default function BombaEletrohidraulica () {
  return (
    <>
      <Head>
        <title>Bomba Motorizada 700 BAR | Hidraumon Máquinas</title>
      </Head>

      <section className="product-info">
        <Container>
          <Row className="py-5">
            <Col lg={12} md={12} sm={12} xs={12}>
              <h2 data-aos="fade-up">
              Bomba
                <br />
                <strong>Eletro-Hidráulica</strong>
              </h2>
              <br />
              <p data-aos="fade-up" data-aos-delay="300">
              </p>
            </Col>
          </Row>
          <Row>
            <Col lg={6} md={6} sm={12} xs={12} className="mb-5" data-aos="fade-up" data-aos-delay="500">
              <LightGallery mode="lg-fade" plugins={[lgZoom]}>
                <a
                  className="gallery-item"
                  data-src="/images/produtos/cilindros-e-bombas/bomba-eletrohidraulica/1.png"
                  data-sub-html="<h4>Bomba Eletrohidráulica</h4><p>Hidraumon Máquinas</p>"
                >
                  <img
                    className="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-1 img-fluid"
                    src="/images/produtos/cilindros-e-bombas/bomba-eletrohidraulica/1.png"
                    />
                </a>
              </LightGallery>
            </Col>
            <Col lg={6} md={6} sm={12} xs={12} data-aos="fade-up" data-aos-delay="800">
              <ProductInfo
                lineone={[
                  <BiRadioCircle />,
                  " Consulte mais detalhes com um de nossos vendedores: (17)98201-0001 ou (17)3226-9090."
                ]}
                />
            </Col>
          </Row>
        </Container>
      </section>
      </>
  );
}
