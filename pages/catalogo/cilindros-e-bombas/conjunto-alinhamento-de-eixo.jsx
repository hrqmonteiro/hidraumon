
import { Col, Container, Row, } from "react-bootstrap";
import Head from "next/head";
import ProductInfo from "../../../components/UI/ProductInfo";
import { BiRadioCircle } from "react-icons/bi";
import lgZoom from "lightgallery/plugins/zoom";
import dynamic from "next/dynamic";

const LightGallery = dynamic(() => import
('lightgallery/react'), {
    ssr: false
  });

export default function Conjunto () {
  return (
    <>
      <Head>
        <title>Conjunto de Alinhamento de Eixo | Hidraumon Máquinas</title>
      </Head>

      <section className="product-info">
        <Container>
          <Row className="py-5">
            <Col lg={12} md={12} sm={12} xs={12}>
              <h2 data-aos="fade-up">
              Conjunto de
                <br />
                <strong>Alinhamento de Eixo</strong>
              </h2>
              <br />
              <p data-aos="fade-up" data-aos-delay="300">
              </p>
            </Col>
          </Row>
          <Row>
            <Col lg={6} md={6} sm={12} xs={12} className="mb-5" data-aos="fade-up" data-aos-delay="500">
              <LightGallery mode="lg-fade" plugins={[lgZoom]}>
                <a
                  className="gallery-item"
                  data-src="/images/produtos/cilindros-e-bombas/conjunto-alinhamento-de-eixo/1.jpg"
                  data-sub-html="<h4>Conjunto de Alinhamento de Eixo</h4><p>Hidraumon Máquinas</p>"
                >
                  <img
                    className="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-1 img-fluid"
                    src="/images/produtos/cilindros-e-bombas/conjunto-alinhamento-de-eixo/1.jpg"
                    />
                </a>
              </LightGallery>
            </Col>
            <Col lg={6} md={6} sm={12} xs={12} data-aos="fade-up" data-aos-delay="800">
              <ProductInfo
                lineone={[
                  <BiRadioCircle />,
                  " Capacidade cilindro: 100 toneladas"
                ]}
                linetwo={[
                  <BiRadioCircle />,
                  " Altura fechado cilindro: 340mm"
                ]}
                linethree={[
                  <BiRadioCircle />,
                  " Curso cilindro: 155mm"
                ]}
                linefour={[
                  <BiRadioCircle />,
                  " Diâmetro haste: 4''"
                ]}
                linefive={[
                  <BiRadioCircle />,
                  " Pressão bomba: 700BAR"
                ]}
                linesix={[
                  <BiRadioCircle />,
                  " Acionamento: Válvula hidropneumática direcional de alavanca"
                ]}
                lineseven={[
                  <BiRadioCircle />,
                  " Compressor recomendado: 15 pés/175lbs"
                ]}
                lineeight={[
                  <BiRadioCircle />,
                  " Consumo de ar: 20PCM"
                ]}
                linenine={[
                  <BiRadioCircle />,
                  " Reservatório de óleo: 20 litros/Óleo 32W"
                ]}
                lineten={[
                  <BiRadioCircle />,
                  " Peso: 180kg"
                ]}
                lineeleven={[
                  <BiRadioCircle />,
                  " Garantia: 1 ano"
                ]}
                />
            </Col>
          </Row>
        </Container>
      </section>
      </>
  );
}
