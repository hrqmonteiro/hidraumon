import { Col, Container, Row, } from "react-bootstrap";
import Head from "next/head";
import ProductInfo from "../../../components/UI/ProductInfo";
import { BiRadioCircle } from "react-icons/bi";
import lgZoom from "lightgallery/plugins/zoom";
import dynamic from "next/dynamic";

const LightGallery = dynamic(() => import
('lightgallery/react'), {
    ssr: false
  });

export default function Cart () {
  return (
    <>
      <Head>
        <title>Carrinho de Retirar Câmbio c/ Bandeja | Hidraumon Máquinas</title>
      </Head>

      <section className="product-info">
        <Container>
          <Row className="py-5">
            <Col lg={12} md={12} sm={12} xs={12}>
              <h2 data-aos="fade-up">
              Carrinho de
                <br />
                <strong>Retirar câmbio</strong> <em>c/ Bandeja</em>
              </h2>
              <br />
              <p data-aos="fade-up" data-aos-delay="300">
              Desenvolvido para retirar câmbio de caminhões.
                <br />
                Possui bandeja articulável, regulagem, inclinação 15 graus e rodas em duas opções: ferro fundido e polipropileno.
              </p>
            </Col>
          </Row>
          <Row>
            <Col lg={6} md={6} sm={12} xs={12} className="mb-5" data-aos="fade-up" data-aos-delay="500">
              <LightGallery mode="lg-fade" plugins={[lgZoom]}>
                <a
                  className="gallery-item"
                  data-src="/images/produtos/carrinhos/carrinho-c-bandeja/1.jpg"
                  data-sub-html="<h4>Carrinho de Retirar Câmbio c/ Bandeja</h4><p>Hidraumon Máquinas</p>"
                >
                  <img
                    className="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-1 img-fluid"
                    src="/images/produtos/carrinhos/carrinho-c-bandeja/1.jpg"
                    />
                </a>
                <a
                  className="gallery-item"
                  data-src="/images/produtos/carrinhos/carrinho-c-bandeja/2.jpg"
                  data-sub-html="<h4>Carrinho de Retirar Câmbio c/ Bandeja</h4><p>Hidraumon Máquinas</p>"
                >
                  <img
                    className="col-lg-6 col-md-6 col-sm-6 col-xs-6 p-1 img-fluid"
                    src="/images/produtos/carrinhos/carrinho-c-bandeja/2.jpg"
                    />
                </a>
                <a

                  className="gallery-item"
                  data-src="/images/produtos/carrinhos/carrinho-c-bandeja/3.jpg"
                  data-sub-html="<h4>Carrinho de Retirar Câmbio c/ Bandeja</h4><p>Hidraumon Máquinas</p>"
                >
                  <img
                    className="col-lg-6 col-md-6 col-sm-6 col-xs-6 p-1 img-fluid"
                    src="/images/produtos/carrinhos/carrinho-c-bandeja/3.jpg"
                    />
                </a>
              </LightGallery>
            </Col>

            <Col lg={6} md={6} sm={12} xs={12} data-aos="fade-up" data-aos-delay="800">
              <ProductInfo
                lineone={[
                  <BiRadioCircle />,
                  " Capacidade: 1 tonelada"
                ]}
                linetwo={[
                  <BiRadioCircle />,
                  " Comprimento: 1250mm"
                ]}
                linethree={[
                  <BiRadioCircle />,
                  " Largura: 800mm"
                ]}
                linefour={[
                  <BiRadioCircle />,
                  " Altura: mínima 180mm / máxima 780mm"
                ]}
                linefive={[
                  <BiRadioCircle />,
                  " Óleo utilizado: 32W"
                ]}
                linesix={[
                  <BiRadioCircle />,
                  " Peso: 103kg"
                ]}
                lineseven={[
                  <BiRadioCircle />,
                  " Garantia: 1 ano"
                ]}
                reference="SHCB"
                />
            </Col>
          </Row>
        </Container>
      </section>
      </>
  );
}
