import { Col, Container, Row } from "react-bootstrap";
import Head from "next/head";
import ProductInfo from "../../../components/UI/ProductInfo";
import { BiRadioCircle } from "react-icons/bi";
import lgZoom from "lightgallery/plugins/zoom";
import dynamic from "next/dynamic";

const LightGallery = dynamic(() => import("lightgallery/react"), {
  ssr: false,
});

export default function SacaTwo() {
  return (
    <>
      <Head>
        <title>
          Saca-Pinos da Manga de Eixo Manual 45 Toneladas | Hidraumon Máquinas
        </title>
      </Head>

      <section className="product-info">
        <Container>
          <Row className="py-5">
            <Col lg={12} md={12} sm={12} xs={12}>
              <h2 data-aos="fade-up">
                Saca-Pinos da Manga de
                <br />
                <strong>Eixo Manual 45 Toneladas</strong>
              </h2>
              <br />
              <p data-aos="fade-up" data-aos-delay="300">
                Desenvolvido para sacar pino da manga de eixo de caminhões
                diretamente no veículo, com retorno por gravidade, bomba
                hidráulica manual e 2 injetores (rápido e lento).
                <br />
                Disponível nos modelos manual e hidropneumático.
              </p>
            </Col>
          </Row>
          <Row>
            <Col
              lg={6}
              md={6}
              sm={12}
              xs={12}
              className="mb-5"
              data-aos="fade-up"
              data-aos-delay="500"
            >
              <LightGallery mode="lg-fade" plugins={[lgZoom]}>
                <a
                  className="gallery-item"
                  data-src="/images/produtos/saca-pinos/saca-pinos-da-manga-de-eixo-manual-45-toneladas/1.jpg"
                  data-sub-html="<h4>Saca-Pinos da Manga de Eixo Manual 45 Toneladas</h4><p>Hidraumon Máquinas</p>"
                >
                  <img
                    className="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-1 img-fluid"
                    src="/images/produtos/saca-pinos/saca-pinos-da-manga-de-eixo-manual-45-toneladas/1.jpg"
                  />
                </a>
                <a
                  className="gallery-item"
                  data-src="/images/produtos/saca-pinos/saca-pinos-da-manga-de-eixo-manual-45-toneladas/2.jpg"
                  data-sub-html="<h4>Saca-Pinos da Manga de Eixo Manual 45 Toneladas</h4><p>Hidraumon Máquinas</p>"
                >
                  <img
                    className="col-lg-6 col-md-6 col-sm-6 col-xs-6 p-1 img-fluid"
                    src="/images/produtos/saca-pinos/saca-pinos-da-manga-de-eixo-manual-45-toneladas/2.jpg"
                  />
                </a>
                <a
                  className="gallery-item"
                  data-src="/images/produtos/saca-pinos/saca-pinos-da-manga-de-eixo-manual-45-toneladas/3.jpg"
                  data-sub-html="<h4>Saca-Pinos da Manga de Eixo Manual 45 Toneladas</h4><p>Hidraumon Máquinas</p>"
                >
                  <img
                    className="col-lg-6 col-md-6 col-sm-6 col-xs-6 p-1 img-fluid"
                    src="/images/produtos/saca-pinos/saca-pinos-da-manga-de-eixo-manual-45-toneladas/3.jpg"
                  />
                </a>
              </LightGallery>
            </Col>
            <Col
              lg={6}
              md={6}
              sm={12}
              xs={12}
              data-aos="fade-up"
              data-aos-delay="800"
            >
              <ProductInfo
                lineone={[<BiRadioCircle />, " Capacidade: 45 toneladas"]}
                linetwo={[<BiRadioCircle />, " Altura: 700mm"]}
                linethree={[<BiRadioCircle />, " Largura entre coluna: 290mm"]}
                linefour={[<BiRadioCircle />, " Encaixe: 40mm"]}
                linefive={[<BiRadioCircle />, " Curso: 130mm"]}
                linesix={[<BiRadioCircle />, " Fuso: 1''1/4"]}
                lineseven={[<BiRadioCircle />, " Peso: 65kg"]}
                lineeight={[<BiRadioCircle />, " Garantia: 1 ano"]}
                reference="SCPN"
              />
            </Col>
          </Row>
        </Container>
      </section>
    </>
  );
}
