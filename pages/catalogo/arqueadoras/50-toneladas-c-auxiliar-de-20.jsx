import { Col, Container, Row, } from "react-bootstrap";
import Head from "next/head";
import ProductInfo from "../../../components/UI/ProductInfo";
import { BiRadioCircle } from "react-icons/bi";
import lgZoom from "lightgallery/plugins/zoom";
import dynamic from "next/dynamic";

const LightGallery = dynamic(() => import
('lightgallery/react'), {
    ssr: false
  });

export default function FiftyTonAux () {
  return (
    <>
      <Head>
        <title>Arqueadora de Molas 50 Toneladas c/ Auxiliar de 20 | Hidraumon Máquinas</title>
      </Head>

      <section className="product-info">
        <Container>
          <Row className="py-5">
            <Col lg={12} md={12} sm={12} xs={12}>
              <h2 data-aos="fade-up">
              Arqueadora de Molas
                <br />
                <strong>50 Toneladas</strong> <em>c/ Auxiliar de 20</em>
              </h2>
              <br />
              <p data-aos="fade-up" data-aos-delay="300">
              Desenvolvida para arquear molas a frio até 31mm com eficiência e qualidade.
              </p>
            </Col>
          </Row>
          <Row>
            <Col lg={6} md={6} sm={12} xs={12} className="mb-5" data-aos="fade-up" data-aos-delay="500">
              <LightGallery mode="lg-fade" plugins={[lgZoom]}>
                <a
                  className="gallery-item"
                  data-src="/images/produtos/arqueadoras/50-toneladas-c-auxiliar-de-20/1.jpg"
                  data-sub-html="<h4>Arqueadora de Molas 50 Toneladas c/ Auxiliar de 20</h4><p>Hidraumon Máquinas</p>"
                >
                  <img
                    className="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-1 img-fluid"
                    src="/images/produtos/arqueadoras/50-toneladas-c-auxiliar-de-20/1.jpg"
                    />
                </a>
                <a
                  className="gallery-item"
                  data-src="/images/produtos/arqueadoras/50-toneladas-c-auxiliar-de-20/2.jpg"
                  data-sub-html="<h4>Arqueadora de Molas 50 Toneladas c/ Auxiliar de 20</h4><p>Hidraumon Máquinas</p>"
                >
                  <img
                    className="col-lg-6 col-md-6 col-sm-6 col-xs-6 p-1 img-fluid"
                    src="/images/produtos/arqueadoras/50-toneladas-c-auxiliar-de-20/2.jpg"
                    />
                </a>

              </LightGallery>
            </Col>
            <Col lg={6} md={6} sm={12} xs={12} data-aos="fade-up" data-aos-delay="800">
              <ProductInfo
                lineone={[
                  <BiRadioCircle />,
                  " Pedal de acionamento e retorno do cilindro por molas"
                ]}
                linetwo={[
                  <BiRadioCircle />,
                  " Curso: 100mm"
                ]}
                linethree={[
                  <BiRadioCircle />,
                  " Chave Liga/Desliga"
                ]}
                linefour={[
                  <BiRadioCircle />,
                  " Motor em duas opções: Trifásico e Monofásico"
                ]}
                linefive={[
                  <BiRadioCircle />,
                  " Voltagem: 220V / 380V"
                ]}
                linesix={[
                  <BiRadioCircle />,
                  " Peso: 315kg"
                ]}
                lineseven={[
                  <BiRadioCircle />,
                  " Acompanha prensa auxiliar manual de 20 toneladas fixada na estrutura"
                ]}
                lineeight={[
                  <BiRadioCircle />,
                  " Garantia: 1 ano"
                ]}
                reference="MA50AX20"
                />
            </Col>
          </Row>
        </Container>
      </section>
      </>
  );
}
