import { Col, Container, Row, } from "react-bootstrap";
import Head from "next/head";
import ProductInfo from "../../../components/UI/ProductInfo";
import { BiRadioCircle } from "react-icons/bi";
import lgZoom from "lightgallery/plugins/zoom";
import dynamic from "next/dynamic";

const LightGallery = dynamic(() => import
('lightgallery/react'), {
    ssr: false
  });

export default function FiftyTonDoubleAction () {
  return (
    <>
      <Head>
        <title>Arqueadora de Molas Universal Dupla-Ação 50 Toneladas | Hidraumon Máquinas</title>
      </Head>

      <section className="product-info">
        <Container>
          <Row className="py-5">
            <Col lg={12} md={12} sm={12} xs={12}>
              <h2 data-aos="fade-up">
              Arqueadora de Molas Universal
                <br />
                <strong>Dupla Ação 50 Toneladas</strong>
              </h2>
              <br />
              <p data-aos="fade-up" data-aos-delay="300">
              Equipamento universal 2 em 1: Arqueadora e Prensa.
                <br />
                Desenvolvida para arquear, ajustar e prensar com eficiência e qualidade.
              </p>
            </Col>
          </Row>
          <Row>
            <Col lg={6} md={6} sm={12} xs={12} className="mb-5" data-aos="fade-up" data-aos-delay="500">
              <LightGallery mode="lg-fade" plugins={[lgZoom]}>
                <a
                  className="gallery-item"
                  data-src="/images/produtos/arqueadoras/50-toneladas-dupla-acao/1.jpg"
                  data-sub-html="<h4>Arqueadora de Molas Universal Dupla Ação 50 Toneladas</h4><p>Hidraumon Máquinas</p>"
                >
                  <img
                    className="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-1 img-fluid"
                    src="/images/produtos/arqueadoras/50-toneladas-dupla-acao/1.jpg"
                    />
                </a>
                <a
                  className="gallery-item"
                  data-src="/images/produtos/arqueadoras/50-toneladas-dupla-acao/2.jpg"
                  data-sub-html="<h4>Arqueadora de Molas Universal Dupla Ação 50 Toneladas</h4><p>Hidraumon Máquinas</p>"
                >
                  <img
                    className="col-lg-6 col-md-6 col-sm-6 col-xs-6 p-1 img-fluid"
                    src="/images/produtos/arqueadoras/50-toneladas-dupla-acao/2.jpg"
                    />
                </a>

              </LightGallery>
            </Col>
            <Col lg={6} md={6} sm={12} xs={12} data-aos="fade-up" data-aos-delay="800">
              <ProductInfo
                lineone={[
                  <BiRadioCircle />,
                  " Curso: 160mm"
                ]}
                linetwo={[
                  <BiRadioCircle />,
                  " Vão livre: 270mm"
                ]}
                linethree={[
                  <BiRadioCircle />,
                  " 2 Pedais de acionamento"
                ]}
                linefour={[
                  <BiRadioCircle />,
                  " 2 Jogos de sapatas"
                ]}
                linefive={[
                  <BiRadioCircle />,
                  " Chave Liga/Desliga"
                ]}
                linesix={[
                  <BiRadioCircle />,
                  " Motor nas opções: Trifásico e Monofásico"
                ]}
                lineseven={[
                  <BiRadioCircle />,
                  " 220V / 380V"
                ]}
                lineeight={[
                  <BiRadioCircle />,
                  " 50Hz / 60Hz"
                ]}
                linenine={[
                  <BiRadioCircle />,
                  " Peso: 330kg"
                ]}
                lineten={[
                  <BiRadioCircle />,
                  " Garantia: 1 ano"
                ]}
                reference="MAU2020"
                />
            </Col>
          </Row>
        </Container>
      </section>
      </>
  );
}
