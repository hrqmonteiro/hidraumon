import { Col, Container, Row, } from "react-bootstrap";
import Head from "next/head";
import ProductInfo from "../../../components/UI/ProductInfo";
import Lightbox from "../../../components/UI/Lightbox";
import { BiRadioCircle } from "react-icons/bi";

export default function FiftyTon () {
  return (
    <>
      <Head>
        <title>Arqueadora de Molas 50 Toneladas | Hidraumon Máquinas</title>
      </Head>

      <section className="product-info">
        <Container>
          <Row className="py-5">
            <Col lg={12} md={12} sm={12} xs={12}>
              <h2 data-aos="fade-up">
              Arqueadora de Molas
                <br />
                <strong>50 Toneladas</strong>
              </h2>
              <br />
              <p data-aos="fade-up" data-aos-delay="300">
              Desenvolvida para arquear molas a frio até 31mm com eficiência e qualidade.
              </p>
            </Col>
          </Row>
          <Row>
            <Col lg={6} md={6} sm={12} xs={12} className="mb-5" data-aos="fade-up" data-aos-delay="500">
              <Lightbox
                imgone="/images/produtos/arqueadoras/50-toneladas/1.webp"
                descone="<h4>Arqueadora de Molas 50 Toneladas</h4> <p>Hidraumon Máquinas</p>"
                imgtwo="/images/produtos/arqueadoras/50-toneladas/2.jpg"
                imgthree="/images/produtos/arqueadoras/50-toneladas/3.webp"
              />
            </Col>
            <Col lg={6} md={6} sm={12} xs={12} data-aos="fade-up" data-aos-delay="800">
              <ProductInfo
                imgone="/images/produtos/arqueadoras/50-toneladas/1.webp"
                lineone={[
                  <BiRadioCircle />,
                  " Pedal de acionamento e retorno do cilindro por molas"
                ]}
                linetwo={[
                  <BiRadioCircle />,
                  " Curso: 100mm"
                ]}
                linethree={[
                  <BiRadioCircle />,
                  " Chave Liga/Desliga"
                ]}
                linefour={[
                  <BiRadioCircle />,
                  " Motor em duas opções: Trifásico e Monofásico"
                ]}
                linefive={[
                  <BiRadioCircle />,
                  " Voltagem: 220V / 380V"
                ]}
                linesix={[
                  <BiRadioCircle />,
                  " Peso: 270kg"
                ]}
                lineseven={[
                  <BiRadioCircle />,
                  " Garantia: 1 ano"
                ]}
                reference="MA50"
                />
            </Col>
          </Row>
        </Container>
      </section>
      </>
  );
}
