import { Col, Container, Row, } from "react-bootstrap";
import Head from "next/head";
import ProductInfo from "../../../components/UI/ProductInfo";
import { BiRadioCircle } from "react-icons/bi";
import lgZoom from "lightgallery/plugins/zoom";
import dynamic from "next/dynamic";

const LightGallery = dynamic(() => import
('lightgallery/react'), {
    ssr: false
  });

export default function DoubleForty () {
  return (
    <>
      <Head>
        <title>Expansor Dupla Ação 40 Toneladas | Hidraumon Máquinas</title>
      </Head>

      <section className="product-info">
        <Container>
          <Row className="py-5">
            <Col lg={12} md={12} sm={12} xs={12}>
              <h2 data-aos="fade-up">
              Expansor Dupla Ação
                <br />
                <strong>40 Toneladas</strong>
              </h2>
              <br />
              <p data-aos="fade-up" data-aos-delay="300">
                Bomba acionamento manual (700 BAR) dupla ação e 2 injetores (40 avanço e 20 retorno).
                <br />
                Acessórios: 2 sapatas de avanço, 2 sapatas de retorno, 1 tubo maciço 300mm.
              </p>
            </Col>
          </Row>
          <Row>
            <Col lg={6} md={6} sm={12} xs={12} className="mb-5" data-aos="fade-up" data-aos-delay="500">
              <LightGallery mode="lg-fade" plugins={[lgZoom]}>
                <a
                  className="gallery-item"
                  data-src="/images/produtos/expansores/dupla-acao-40-toneladas/1.jpg"
                  data-sub-html="<h4>Expansor Dupla Ação 40 Toneladas</h4><p>Hidraumon Máquinas</p>"
                >
                  <img
                    className="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-1 img-fluid"
                    src="/images/produtos/expansores/dupla-acao-40-toneladas/1.jpg"
                    />
                </a>
                <a
                  className="gallery-item"
                  data-src="/images/produtos/expansores/dupla-acao-40-toneladas/2.jpg"
                  data-sub-html="<h4>Expansor Dupla Ação 40 Toneladas</h4><p>Hidraumon Máquinas</p>"
                >
                  <img
                    className="col-lg-6 col-md-6 col-sm-6 col-xs-6 p-1 img-fluid"
                    src="/images/produtos/expansores/dupla-acao-40-toneladas/2.jpg"
                    />
                </a>
              </LightGallery>
            </Col>
            <Col lg={6} md={6} sm={12} xs={12} data-aos="fade-up" data-aos-delay="800">

              <ProductInfo
                lineone={[
                  <BiRadioCircle />,
                  " Capacidade: 40 toneladas avanço e 20 toneladas de retorno"
                ]}
                linetwo={[
                  <BiRadioCircle />,
                  " Altura do pistão: 500mm"
                ]}
                linethree={[
                  <BiRadioCircle />,
                  " Curso: 240mm"
                ]}
                linefour={[
                  <BiRadioCircle />,
                  " Altura total: 740mm"
                ]}
                linefive={[
                  <BiRadioCircle />,
                  " Mangueiras: 2m"
                ]}
                linesix={[
                  <BiRadioCircle />,
                  " Óleo: 32W"
                ]}
                lineseven={[
                  <BiRadioCircle />,
                  " Peso: 51kg"
                ]}
                lineeight={[
                  <BiRadioCircle />,
                  " Garantia: 1 ano"
                ]}
                reference="EDA40" 
                />
            </Col>
          </Row>
        </Container>
      </section>
      </>
  );
}
