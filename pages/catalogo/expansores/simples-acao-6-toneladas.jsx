import { Col, Container, Row, } from "react-bootstrap";
import Head from "next/head";
import ProductInfo from "../../../components/UI/ProductInfo";
import { BiRadioCircle } from "react-icons/bi";
import lgZoom from "lightgallery/plugins/zoom";
import dynamic from "next/dynamic";

const LightGallery = dynamic(() => import
('lightgallery/react'), {
    ssr: false
  });

export default function SimplesSix () {
  return (
    <>
      <Head>
        <title>Expansor Simples Ação 6 Toneladas | Hidraumon Máquinas</title>
      </Head>

      <section className="product-info">
        <Container>
          <Row className="py-5">
            <Col lg={12} md={12} sm={12} xs={12}>
              <h2 data-aos="fade-up">
              Expansor Simples Ação
                <br />
                <strong>6 Toneladas</strong>
              </h2>
              <br />
              <p data-aos="fade-up" data-aos-delay="300">
              Ideal para pequenos reparos em latarias, chassis e monoblocos.
                <br />
                Conjunto composto por cilindro hidráulico simples ação com retorno por molas, mangueira de alta resistência e bomba de injeção manual. Acompanha conjunto de canos prolongadores e sapatas.
                <br />
                Acompanha conjunto de canos prolongadores e sapatas.
                <br />
                Acessórios: 2 sapatas de apoio, 1 união para os tubos, 3 luvas chanfradas (100mm, 250mm e 400mm) e 2 prolongadores (200mm e 300mm).
              </p>
            </Col>
          </Row>
          <Row>
            <Col lg={6} md={6} sm={12} xs={12} className="mb-5" data-aos="fade-up" data-aos-delay="500">
              <LightGallery mode="lg-fade" plugins={[lgZoom]}>
                <a
                  className="gallery-item"
                  data-src="/images/produtos/expansores/simples-acao-6-toneladas/1.jpg"
                  data-sub-html="<h4>Expansor Simples Ação 6 Toneladas</h4><p>Hidraumon Máquinas</p>"
                >
                  <img
                    className="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-1 img-fluid"
                    src="/images/produtos/expansores/simples-acao-6-toneladas/1.jpg"
                    />
                </a>
              </LightGallery>
            </Col>
            <Col lg={6} md={6} sm={12} xs={12} data-aos="fade-up" data-aos-delay="800">

              <ProductInfo
                lineone={[
                  <BiRadioCircle />,
                  " Capacidade: 6 toneladas"
                ]}
                linetwo={[
                  <BiRadioCircle />,
                  " Altura do pistão: 350mm"
                ]}
                linethree={[
                  <BiRadioCircle />,
                  " Curso: 130mm"
                ]}
                linefour={[
                  <BiRadioCircle />,
                  " Altura total: 495mm"
                ]}
                linefive={[
                  <BiRadioCircle />,
                  " Mangueiras: 1,5m"
                ]}
                linesix={[
                  <BiRadioCircle />,
                  " Óleo: 32W"
                ]}
                lineseven={[
                  <BiRadioCircle />,
                  " Peso: 17kg"
                ]}
                lineeight={[
                  <BiRadioCircle />,
                  " Garantia: 1 ano"
                ]}
                reference="EX6" 
                />
            </Col>
          </Row>
        </Container>
      </section>
      </>
  );
}
