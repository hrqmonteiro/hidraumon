import { Col, Container, Row, } from "react-bootstrap";
import Head from "next/head";
import ProductInfo from "../../../components/UI/ProductInfo";
import { BiRadioCircle } from "react-icons/bi";
import lgZoom from "lightgallery/plugins/zoom";
import dynamic from "next/dynamic";

const LightGallery = dynamic(() => import
('lightgallery/react'), {
    ssr: false
  });

export default function SimplesTwenty () {
  return (
    <>
      <Head>
        <title>Expansor Simples Ação 20 Toneladas | Hidraumon Máquinas</title>
      </Head>

      <section className="product-info">
        <Container>
          <Row className="py-5">
            <Col lg={12} md={12} sm={12} xs={12}>
              <h2 data-aos="fade-up">
              Expansor Simples Ação
                <br />
                <strong>20 Toneladas</strong>
              </h2>
              <br />
              <p data-aos="fade-up" data-aos-delay="300">
              Ideal para pequenos reparos em latarias, chassis e caminhões.
                <br />
                Colocação do eixo no lugar, remoção da balança travada e colocação dos pinos de tirante.
                <br />
                Conjunto composto por cilindro hidráulico simples ação, retorno por gravidade, mangueira de alta resistência e bomba de injeção manual. Acompanha conjunto de canos prolongadores e sapatas.
                <br />
                Acessórios: 2 sapatas de apoio, 1 união p/ os tubos, 3 luvas chanfradas (100mm, 100mm e 300 mm) e 3 prolongadores (100mm, 200mm e 300 mm).
              </p>
            </Col>
          </Row>
          <Row>
            <Col lg={6} md={6} sm={12} xs={12} className="mb-5" data-aos="fade-up" data-aos-delay="500">
              <LightGallery mode="lg-fade" plugins={[lgZoom]}>
                <a
                  className="gallery-item"
                  data-src="/images/produtos/expansores/simples-acao-20-toneladas/1.jpg"
                  data-sub-html="<h4>Expansor Simples Ação 20 Toneladas</h4><p>Hidraumon Máquinas</p>"
                >
                  <img
                    className="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-1 img-fluid"
                    src="/images/produtos/expansores/simples-acao-20-toneladas/1.jpg"
                    />
                </a>
                <a
                  className="gallery-item"
                  data-src="/images/produtos/expansores/simples-acao-20-toneladas/2.jpg"
                  data-sub-html="<h4>Expansor Simples Ação 20 Toneladas</h4><p>Hidraumon Máquinas</p>"
                >
                  <img
                    className="col-lg-6 col-md-6 col-sm-6 col-xs-6 p-1 img-fluid"
                    src="/images/produtos/expansores/simples-acao-20-toneladas/2.jpg"
                    />
                </a>
              </LightGallery>
            </Col>
            <Col lg={6} md={6} sm={12} xs={12} data-aos="fade-up" data-aos-delay="800">

              <ProductInfo
                lineone={[
                  <BiRadioCircle />,
                  " Capacidade: 20 toneladas"
                ]}
                linetwo={[
                  <BiRadioCircle />,
                  " Altura do pistão: 350mm"
                ]}
                linethree={[
                  <BiRadioCircle />,
                  " Curso: 145mm"
                ]}
                linefour={[
                  <BiRadioCircle />,
                  " Altura total: 495mm"
                ]}
                linefive={[
                  <BiRadioCircle />,
                  " Mangueiras: 1,5m"
                ]}
                linesix={[
                  <BiRadioCircle />,
                  " Óleo: 32W"
                ]}
                lineseven={[
                  <BiRadioCircle />,
                  " Peso: 28kg"
                ]}
                lineeight={[
                  <BiRadioCircle />,
                  " Garantia: 1 ano"
                ]}
                reference="EX20" 
                />
            </Col>
          </Row>
        </Container>
      </section>
      </>
  );
}
